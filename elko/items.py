# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import MapCompose


def filter_string(value):
    if value :
        return value

class ElkoItem(scrapy.Item):

    title = scrapy.Field()
    price = scrapy.Field()
    code_elko = scrapy.Field()
    code_manufacture = scrapy.Field()
    na_sklade = scrapy.Field()
    manufacture = scrapy.Field()


class ProductItems(scrapy.Item):
    title = scrapy.Field(input_processor=MapCompose(filter_string))
    key = scrapy.Field()
    value = scrapy.Field()












